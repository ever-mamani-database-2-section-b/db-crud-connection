# Proyecto CRUD de Propietarios

Este proyecto Java muestra un ejemplo de operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en una base de datos MySQL. Utiliza JDBC para conectarse a la base de datos y realiza las operaciones en una tabla de propietarios.

Este proyecto fue creado con IntelliJ IDEA y utiliza Maven para administrar las dependencias (mysql).

## Estructura del Proyecto

El proyecto se divide en tres clases principales:

1. DatabaseConnection
    - Descripción: Esta clase se encarga de establecer la conexión con la base de datos MySQL.
    - Métodos:
        - getConnection(): Método estático que devuelve una instancia de conexión a la base de datos.

2. Propietario
    - Descripción: Esta clase representa un propietario y se utiliza para crear objetos Propietario con información del propietario.
    - Constructores:
        - Propietario(int id, String nombre, String apellido, Date fechaNacimiento, String telefono, String correoElectronico): Constructor para cargar un propietario existente desde la base de datos.
        - Propietario(String nombre, String apellido, Date fechaNacimiento, String telefono, String correoElectronico): Constructor para crear un nuevo propietario.

3. PropietarioDAO
    - Descripción: Esta clase se encarga de realizar operaciones CRUD en la tabla de propietarios de la base de datos.
    - Métodos:
        - insertarPropietario(Propietario propietario): Inserta un nuevo propietario en la base de datos.
        - actualizarPropietario(Propietario propietario): Actualiza la información de un propietario existente en la base de datos.
        - eliminarPropietario(int id_Propietario): Elimina un propietario de la base de datos por su ID.
        - obtenerPropietarioPorId(int id_Propietario): Obtiene un propietario de la base de datos por su ID.
        - obtenerPropietarios(): Obtiene todos los propietarios de la base de datos y los devuelve en una lista.

## Uso del Proyecto

El archivo Main demuestra el uso del proyecto con ejemplos de inserción, actualización, eliminación y selección de propietarios.
