package university.jala;

import university.jala.conexionDB.Propietario;
import university.jala.conexionDB.PropietarioDAO;
import university.jala.conexionDB.TablaUtil;
import java.sql.Date;
import java.util.List;

/**
 * La clase principal del programa.
 *
 * @author Ever Mamani Vicente.
 */
public class Main {

  public static void main(String[] args) {

    PropietarioDAO propietarioDAO = new PropietarioDAO();

    Propietario nuevoPropietario = new Propietario(
        15,
        "Juan",
        "Perez",
        Date.valueOf("1980-05-15"),
        "123-456-7890",
        "juan@example.com"
    );

    // INSERT
    propietarioDAO.insertarPropietario(nuevoPropietario);

    // OBTENER POR ID
    Propietario propietario = propietarioDAO.obtenerPropietarioPorId(1);
    System.out.printf("Propietario encontrado por ID: %s%n", propietario);

    // ACTUALIZAR
    propietario.setCorreoElectronico("nuevo_correo@example.com");
    propietario.setTelefono("987-654-3210");
    propietarioDAO.actualizarPropietario(propietario);

    // ELIMINAR POR ID
    propietarioDAO.eliminarPropietario(13);

    // OBTENER TODOS LOS PROPIETARIOS
    List<Propietario> propietarios = propietarioDAO.obtenerPropietarios();
    TablaUtil.mostrarTabla(propietarios);
  }
}
