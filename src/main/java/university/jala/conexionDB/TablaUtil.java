package university.jala.conexionDB;

import java.util.List;

/**
 * Esta clase proporciona métodos para mostrar una lista de objetos en forma de tabla en la consola.
 *
 * @author Ever Mamani Vicente.
 */
public class TablaUtil {

    /**
     * Muestra una lista de objetos en forma de tabla en la consola.
     *
     * @param listaObjetos La lista de objetos que se mostrarán en la tabla.
     * @param columnWidth El ancho deseado para cada columna.
     */
    public static <T> void mostrarTabla(List<T> listaObjetos) {

        Class<?> clase = listaObjetos.get(0).getClass();
        java.lang.reflect.Field[] campos = clase.getDeclaredFields();

        System.out.println();

        for (java.lang.reflect.Field campo : campos) {
            System.out.print(padRight(campo.getName(), 20));
        }
        System.out.println();

        for (T objeto : listaObjetos) {
            for (java.lang.reflect.Field campo : campos) {
                campo.setAccessible(true);
                try {
                    Object valorCampo = campo.get(objeto);
                    System.out.print(padRight(String.valueOf(valorCampo), 20));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            System.out.println();
        }
    }

    private static String padRight(String s, int width) {
        return String.format("%-" + width + "s", s);
    }
}
