package university.jala.conexionDB;


import java.sql.Date;

/**
 * La clase Propietario representa un registro de la tabla Propietario.
 *
 * @author Ever Mamani Vicente.
 */
public class Propietario {

  private int id_Propietario;
  private String Nombre;
  private String Apellido;
  private Date FechaNacimiento;
  private String Telefono;
  private String CorreoElectronico;

  public Propietario(int id_Propietario, String Nombre, String Apellido, Date FechaNacimiento,
      String Telefono, String CorreoElectronico) {
    this.id_Propietario = id_Propietario;
    this.Nombre = Nombre;
    this.Apellido = Apellido;
    this.FechaNacimiento = FechaNacimiento;
    this.Telefono = Telefono;
    this.CorreoElectronico = CorreoElectronico;
  }

  public Propietario(String Nombre, String Apellido, Date FechaNacimiento, String Telefono,
      String CorreoElectronico) {
    this.Nombre = Nombre;
    this.Apellido = Apellido;
    this.FechaNacimiento = FechaNacimiento;
    this.Telefono = Telefono;
    this.CorreoElectronico = CorreoElectronico;
  }

  public int getId() {
    return id_Propietario;
  }

  public void setId_Propietario(int id_Propietario) {
    this.id_Propietario = id_Propietario;
  }

  public String getNombre() {
    return Nombre;
  }

  public void setNombre(String Nombre) {
    this.Nombre = Nombre;
  }

  public String getApellido() {
    return Apellido;
  }

  public void setApellido(String Apellido) {
    this.Apellido = Apellido;
  }

  public Date getFechaNacimiento() {
    return FechaNacimiento;
  }

  public void setFechaNacimiento(Date FechaNacimiento) {
    this.FechaNacimiento = FechaNacimiento;
  }

  public String getTelefono() {
    return Telefono;
  }

  public void setTelefono(String Telefono) {
    this.Telefono = Telefono;
  }

  public String getCorreoElectronico() {
    return CorreoElectronico;
  }

  public void setCorreoElectronico(String CorreoElectronico) {
    this.CorreoElectronico = CorreoElectronico;
  }

  @Override
  public String toString() {
    return "Propietario{" + "id_Propietario=" + id_Propietario + ", Nombre=" + Nombre
        + ", Apellido=" + Apellido + ", FechaNacimiento=" + FechaNacimiento + ", Telefono="
        + Telefono + ", CorreoElectronico=" + CorreoElectronico + '}';
  }
}
