package university.jala.conexionDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * La clase PropietarioDAO proporciona métodos para interactuar con la tabla de propietarios en la
 * base de datos. Permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en registros
 * de propietarios.
 */
public class PropietarioDAO {

  private final String INSERT_QUERY = "INSERT INTO Propietario (id_Propietario, Nombre, Apellido, FechaNacimiento, Telefono, CorreoElectronico) VALUES (?, ?, ?, ?, ?, ?)";
  private final String UPDATE_QUERY = "UPDATE Propietario SET Nombre=?, Apellido=?, FechaNacimiento=?, Telefono=?, CorreoElectronico=? WHERE id_Propietario=?";
  private final String DELETE_QUERY = "DELETE FROM Propietario WHERE id_Propietario=?";
  private final String SELECT_ALL_QUERY = "SELECT * FROM Propietario";
  private final String SELECT_BY_ID_QUERY = "SELECT * FROM Propietario WHERE id_Propietario=?";

  private Connection connection;

  /**
   * Constructor que inicializa la conexión a la base de datos.
   */
  public PropietarioDAO() {
    connection = DatabaseConnection.getConnection();
  }

  /**
   * Inserta un nuevo propietario en la base de datos.
   *
   * @param propietario El objeto Propietario que se va a insertar.
   */
  public void insertarPropietario(Propietario propietario) {
    try (PreparedStatement stmt = connection.prepareStatement(INSERT_QUERY)) {
      stmt.setInt(1, propietario.getId());
      stmt.setString(2, propietario.getNombre());
      stmt.setString(3, propietario.getApellido());
      stmt.setDate(4, propietario.getFechaNacimiento());
      stmt.setString(5, propietario.getTelefono());
      stmt.setString(6, propietario.getCorreoElectronico());
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Actualiza la información de un propietario existente en la base de datos.
   *
   * @param propietario El objeto Propietario con la información actualizada.
   */
  public void actualizarPropietario(Propietario propietario) {
    try (PreparedStatement stmt = connection.prepareStatement(UPDATE_QUERY)) {
      stmt.setString(1, propietario.getNombre());
      stmt.setString(2, propietario.getApellido());
      stmt.setDate(3, propietario.getFechaNacimiento());
      stmt.setString(4, propietario.getTelefono());
      stmt.setString(5, propietario.getCorreoElectronico());
      stmt.setInt(6, propietario.getId());
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Elimina un propietario de la base de datos por su ID.
   *
   * @param id_Propietario El ID del propietario que se va a eliminar.
   */
  public void eliminarPropietario(int id_Propietario) {
    try (PreparedStatement stmt = connection.prepareStatement(DELETE_QUERY)) {
      stmt.setInt(1, id_Propietario);
      stmt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Obtiene todos los propietarios de la base de datos y los devuelve en una lista.
   *
   * @return Una lista de objetos Propietario que representan a todos los propietarios en la base de
   * datos.
   */
  public List<Propietario> obtenerPropietarios() {
    List<Propietario> propietarios = new ArrayList<>();
    try (PreparedStatement stmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Propietario propietario = new Propietario(rs.getInt("id_Propietario"),
            rs.getString("Nombre"), rs.getString("Apellido"), rs.getDate("FechaNacimiento"),
            rs.getString("Telefono"), rs.getString("CorreoElectronico"));
        propietarios.add(propietario);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return propietarios;
  }

  /**
   * Obtiene un propietario de la base de datos por su ID.
   *
   * @param id_Propietario El ID del propietario que se desea obtener.
   * @return El objeto Propietario correspondiente al ID proporcionado, o null si no se encuentra.
   */
  public Propietario obtenerPropietarioPorId(int id_Propietario) {
    Propietario propietario = null;
    try (PreparedStatement stmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
      stmt.setInt(1, id_Propietario);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        propietario = new Propietario(rs.getInt("id_Propietario"), rs.getString("Nombre"),
            rs.getString("Apellido"), rs.getDate("FechaNacimiento"), rs.getString("Telefono"),
            rs.getString("CorreoElectronico"));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return propietario;
  }
}
