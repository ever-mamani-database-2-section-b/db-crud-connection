-- Active: 1694029070785@@127.0.0.1@3306@CondominioDB
CREATE USER userCondominio IDENTIFIED BY 'userCondominio123';
-- Otorgamiento de permisos de CRUD en la base de datos "CondominioDB"
GRANT SELECT, INSERT, UPDATE, DELETE, TRIGGER, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON CondominioDB.* TO userCondominio;

FLUSH PRIVILEGES;


--
DROP DATABASE CondominioDB;

CREATE DATABASE IF NOT EXISTS CondominioDB;

USE CondominioDB;
CREATE TABLE IF NOT EXISTS Condominio
(
    id_Condominio int PRIMARY KEY AUTO_INCREMENT,
    Nombre        varchar(255),
    Direccion     varchar(255),
    NumeroBloques int
);
CREATE TABLE IF NOT EXISTS Bloque
(
    id_Bloque     int PRIMARY KEY,
    CantidadPisos int,
    id_Condominio int,
    FOREIGN KEY (id_Condominio) REFERENCES Condominio (id_Condominio)
);
CREATE TABLE IF NOT EXISTS Piso
(
    id_Piso   int PRIMARY KEY,
    id_Bloque int,
    FOREIGN KEY (id_Bloque) REFERENCES Bloque (id_Bloque)
);
CREATE TABLE IF NOT EXISTS Propietario
(
    id_Propietario    int PRIMARY KEY,
    Nombre            varchar(50),
    Apellido          varchar(50),
    FechaNacimiento   date,
    Telefono          varchar(20),
    CorreoElectronico varchar(100)
);
CREATE TABLE IF NOT EXISTS Departamento
(
    id_Departamento    int PRIMARY KEY,
    Tamaño             decimal(10, 2),
    NumeroDepartamento int,
    NumHabitaciones    int,
    Estado             varchar(50),
    id_Piso            int,
    id_Propietario     int,
    id_Estacionamiento int,
    FOREIGN KEY (id_Piso) REFERENCES Piso (id_Piso),
    FOREIGN KEY (id_Propietario) REFERENCES Propietario (id_Propietario)
);
CREATE TABLE IF NOT EXISTS Expensas
(
    id_Expensas      int PRIMARY KEY,
    Monto            decimal(10, 2),
    FechaVencimiento date,
    id_Departamento  int,
    FOREIGN KEY (id_Departamento) REFERENCES Departamento (id_Departamento)
);
CREATE TABLE IF NOT EXISTS Estacionamiento
(
    id_Estacionamiento int PRIMARY KEY,
    id_Departamento    int,
    FOREIGN KEY (id_Departamento) REFERENCES Departamento (id_Departamento)
);
SHOW DATABASES;
SHOW TABLE STATUS FROM CondominioDB;


INSERT INTO Condominio (id_Condominio, Nombre, Direccion, NumeroBloques)
VALUES (1, 'Condominio Los Pinos', 'Calle 123', 5),
       (2, 'Condominio Las Rosas', 'Avenida Principal', 3),
       (3, 'Condominio El Parque', 'Calle Central', 4);

INSERT INTO Bloque (id_Bloque, CantidadPisos, id_Condominio)
VALUES (1, 10, 1),
       (2, 8, 1),
       (3, 5, 2),
       (4, 6, 2),
       (5, 7, 3);

INSERT INTO Piso (id_Piso, id_Bloque)
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 3),
       (6, 3),
       (7, 4),
       (8, 4),
       (9, 5),
       (10, 5);

INSERT INTO Propietario (id_Propietario, Nombre, Apellido, FechaNacimiento,
                         Telefono, CorreoElectronico)
VALUES (1, 'Juan', 'Perez', '1980-05-15', '123-456-7890', 'juan@example.com'),
       (2, 'María', 'López', '1975-08-20', '987-654-3210', 'maria@example.com'),
       (3, 'Carlos', 'González', '1990-03-10', '555-123-4567', 'carlos@example.com'),
       (4, 'Laura', 'Rodriguez', '1985-07-12', '555-987-6543', 'laura@example.com'),
       (5, 'Pedro', 'Martínez', '1978-03-25', '333-444-5678', 'pedro@example.com'),
       (6, 'Ana', 'García', '1992-11-15', '777-111-2222', 'ana@example.com'),
       (7, 'Sofía', 'Lara', '1987-09-02', '555-123-7890', 'sofia@example.com'),
       (8, 'Javier', 'Díaz', '1983-12-30', '555-987-4567', 'javier@example.com'),
       (9, 'Isabel', 'Fernández', '1995-04-18', '333-777-8901', 'isabel@example.com'),
       (10, 'Ricardo', 'Ortega', '1970-01-05', '555-444-1234', 'ricardo@example.com'),
       (11, 'Elena', 'Vargas', '1988-06-22', '555-666-2222', 'elena@example.com'),
       (12, 'Luis', 'Sánchez', '1982-02-15', '333-555-7777', 'luis@example.com'),
       (13, 'Marta', 'Gómez', '1990-07-08', '555-333-8888', 'marta@example.com');

INSERT INTO Departamento (id_Departamento, NumeroDepartamento, Tamaño,
                          NumHabitaciones, Estado, id_Piso, id_Propietario, id_Estacionamiento)
VALUES (1, 101, 80.5, 3, 'Ocupado', 1, 1, 1),
       (2, 102, 65.2, 2, 'Desocupado', 1, 2, NULL),
       (3, 201, 95.0, 4, 'Ocupado', 2, 1, 2),
       (4, 202, 75.8, 3, 'Ocupado', 2, 2, 3),
       (5, 301, 110.0, 5, 'Ocupado', 3, 3, 4),
       (6, 302, 70.3, 2, 'Desocupado', 3, 1, NULL),
       (7, 103, 78.0, 3, 'Ocupado', 1, 3, 5),
       (8, 203, 92.5, 4, 'Ocupado', 2, 3, 6),
       (9, 303, 105.8, 4, 'Ocupado', 3, 2, 7),
       (10, 403, 120.2, 5, 'Ocupado', 4, 4, 8),
       (11, 104, 65.0, 2, 'Desocupado', 1, 4, NULL),
       (12, 204, 80.7, 3, 'Desocupado', 2, 5, NULL),
       (13, 305, 110.5, 4, 'Ocupado', 3, 6, 9),
       (14, 405, 95.2, 3, 'Ocupado', 4, 5, 10),
       (15, 106, 72.4, 2, 'Desocupado', 1, 6, NULL),
       (16, 206, 85.3, 3, 'Ocupado', 2, 7, 11);

INSERT INTO Expensas (id_Expensas, Monto, FechaVencimiento, id_Departamento)
VALUES (1, 250.00, '2023-09-05', 1),
       (2, 180.00, '2023-09-05', 2),
       (3, 300.00, '2023-09-10', 3),
       (4, 220.00, '2023-09-10', 4),
       (5, 350.00, '2023-09-15', 5),
       (6, 200.00, '2023-09-15', 6),
       (7, 280.00, '2023-09-05', 7),
       (8, 320.00, '2023-09-05', 8),
       (9, 360.00, '2023-09-10', 9),
       (10, 240.00, '2023-09-10', 10),
       (11, 300.00, '2023-09-15', 11),
       (12, 280.00, '2023-09-15', 12),
       (13, 320.00, '2023-09-05', 13),
       (14, 360.00, '2023-09-05', 14),
       (15, 240.00, '2023-09-10', 15),
       (16, 300.00, '2023-09-10', 16);

INSERT INTO Estacionamiento (id_Estacionamiento, id_Departamento)
VALUES (1, 1),
       (2, 3),
       (3, 4),
       (4, 6),
       (5, 7),
       (6, 9),
       (7, 11),
       (8, 13),
       (9, 15),
       (10, 7),
       (11, 9),
       (12, 11),
       (13, 13),
       (14, 15);


-- 5 Vistas
-- # 1

DROP VIEW IF EXISTS VistaPropietariosExpensasVencidas;

CREATE VIEW VistaPropietariosBloquesGrandes AS
SELECT P.id_Propietario,
       P.Nombre,
       P.Apellido,
       P.FechaNacimiento,
       P.Telefono,
       P.CorreoElectronico,
       B.CantidadPisos,
       C.Nombre AS NombreCondominio
FROM Propietario P
         INNER JOIN Departamento D ON P.id_Propietario = D.id_Propietario
         INNER JOIN Piso PS ON D.id_Piso = PS.id_Piso
         INNER JOIN Bloque B ON PS.id_Bloque = B.id_Bloque
         INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
WHERE B.CantidadPisos > 5
  AND YEAR(P.FechaNacimiento) < 1980
  AND D.Estado = 'Ocupado';

SELECT *
FROM VistaPropietariosBloquesGrandes;

ALTER VIEW VistaPropietariosBloquesGrandes AS
    SELECT P.id_Propietario,
           P.Nombre,
           P.Apellido,
           B.CantidadPisos,
           C.Nombre AS NombreCondominio,
           D.Estado,
           P.FechaNacimiento,
           P.Telefono,
           P.CorreoElectronico
    FROM Condominio C
             INNER JOIN Bloque B ON C.id_Condominio = B.id_Condominio
             INNER JOIN Piso PS ON B.id_Bloque = PS.id_Bloque
             INNER JOIN Departamento D ON PS.id_Piso = D.id_Piso
             INNER JOIN Propietario P ON D.id_Propietario = P.id_Propietario
    WHERE B.CantidadPisos > 7
      AND YEAR(P.FechaNacimiento) > 1980
      AND D.Estado = 'Desocupado';

SELECT *
FROM VistaPropietariosBloquesGrandes;

-- # 2

CREATE VIEW VistaExpensasVencidas AS
SELECT E.id_Expensas,
       E.Monto,
       E.FechaVencimiento,
       D.NumeroDepartamento,
       D.Estado,
       C.Nombre AS NombreCondominio
FROM Expensas E
         INNER JOIN Departamento D ON E.id_Departamento = D.id_Departamento
         INNER JOIN Piso P ON D.id_Piso = P.id_Piso
         INNER JOIN Bloque B ON P.id_Bloque = B.id_Bloque
         INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
WHERE E.FechaVencimiento < CURDATE()
  AND E.Monto > 200
  AND D.Estado = 'Ocupado';

SELECT *
FROM VistaExpensasVencidas;

ALTER VIEW VistaExpensasVencidas AS
    SELECT E.id_Expensas,
           E.Monto,
           E.FechaVencimiento,
           D.NumeroDepartamento,
           D.Estado,
           C.Nombre                          AS NombreCondominio,
           CONCAT(P.Nombre, ' ', P.Apellido) AS Propietario
    FROM Expensas E
             INNER JOIN Departamento D ON E.id_Departamento = D.id_Departamento
             INNER JOIN Propietario P ON D.id_Propietario = P.id_Propietario
             INNER JOIN Piso PS ON D.id_Piso = PS.id_Piso
             INNER JOIN Bloque B ON PS.id_Bloque = B.id_Bloque
             INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
    WHERE E.FechaVencimiento < CURDATE()
      AND E.Monto BETWEEN 200 AND 500
      AND D.Estado = 'Ocupado';

SELECT *
FROM VistaExpensasVencidas;

SELECT NombreCondominio, COUNT(NumeroDepartamento) AS TotalDepartamentos
FROM VistaExpensasVencidas
GROUP BY NombreCondominio;

-- # 3

CREATE VIEW VistaPropietariosDepartamentosOcupados AS
SELECT P.id_Propietario,
       P.Nombre,
       P.Apellido,
       D.NumeroDepartamento,
       B.CantidadPisos AS Numero_de_piso,
       C.Nombre        AS NombreCondominio
FROM Propietario P
         INNER JOIN Departamento D ON P.id_Propietario = D.id_Propietario
         INNER JOIN Piso PS ON D.id_Piso = PS.id_Piso
         INNER JOIN Bloque B ON PS.id_Bloque = B.id_Bloque
         INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
WHERE D.Estado = 'Ocupado'
  AND B.CantidadPisos > 6
  AND YEAR(P.FechaNacimiento) < 1985;

SELECT *
FROM VistaPropietariosDepartamentosOcupados;

ALTER VIEW VistaPropietariosDepartamentosOcupados AS
    SELECT P.id_Propietario,
           P.Nombre,
           P.Apellido,
           D.NumeroDepartamento,
           B.CantidadPisos,
           C.Nombre AS NombreCondominio,
           D.Estado,
           P.FechaNacimiento,
           P.Telefono,
           P.CorreoElectronico
    FROM Condominio C
             INNER JOIN Bloque B ON C.id_Condominio = B.id_Condominio
             INNER JOIN Piso PS ON B.id_Bloque = PS.id_Bloque
             INNER JOIN Departamento D ON PS.id_Piso = D.id_Piso
             INNER JOIN Propietario P ON D.id_Propietario = P.id_Propietario
    WHERE D.Estado = 'Ocupado'
      AND YEAR(P.FechaNacimiento) < 1985;

SELECT *
FROM VistaPropietariosDepartamentosOcupados;

-- # 4

CREATE VIEW VistaDepartamentosExpensasVencidas AS
SELECT D.id_Departamento,
       D.NumeroDepartamento,
       D.Tamaño,
       D.NumHabitaciones,
       D.Estado,
       E.Monto,
       E.FechaVencimiento,
       C.Nombre AS NombreCondominio
FROM Departamento D
         INNER JOIN Expensas E ON D.id_Departamento = E.id_Departamento
         INNER JOIN Piso P ON D.id_Piso = P.id_Piso
         INNER JOIN Bloque B ON P.id_Bloque = B.id_Bloque
         INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
WHERE E.FechaVencimiento < CURDATE()
  AND C.Direccion = 'Calle 123'
  AND D.Tamaño > 90
  AND E.Monto > 250;
SELECT *
FROM VistaDepartamentosExpensasVencidas;

ALTER VIEW VistaDepartamentosExpensasVencidas AS
    SELECT D.NumeroDepartamento,
           D.Tamaño,
           D.NumHabitaciones,
           D.Estado,
           E.Monto,
           E.FechaVencimiento,
           C.Nombre                                            AS NombreCondominio,
           CASE WHEN E.Monto > 300 THEN 'Alto' ELSE 'Bajo' END AS NivelGasto
    FROM Departamento D
             INNER JOIN Expensas E ON D.id_Departamento = E.id_Departamento
             INNER JOIN Piso P ON D.id_Piso = P.id_Piso
             INNER JOIN Bloque B ON P.id_Bloque = B.id_Bloque
             INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
    WHERE E.FechaVencimiento < CURDATE()
      AND C.Direccion = 'Calle 123'
      AND D.Tamaño > 90;

SELECT *
FROM VistaDepartamentosExpensasVencidas;

-- # 5

ALTER VIEW VistaDepartamentosBloquesGrandes AS
    SELECT D.id_Departamento,
           D.NumeroDepartamento,
           D.Tamaño,
           D.NumHabitaciones,
           D.Estado,
           C.Nombre AS NombreCondominio,
           B.CantidadPisos,
           B.id_Condominio
    FROM Departamento D
             INNER JOIN Piso P ON D.id_Piso = P.id_Piso
             INNER JOIN Bloque B ON P.id_Bloque = B.id_Bloque
             INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
    WHERE B.CantidadPisos > 7
      AND C.Direccion = 'Calle 123'
      AND D.Estado = 'Ocupado'
      AND D.Tamaño > 80;

SELECT *
FROM VistaDepartamentosBloquesGrandes;

ALTER VIEW VistaDepartamentosBloquesGrandes AS
    SELECT D.id_Departamento       AS ID_Departamento,
           D.NumeroDepartamento    AS Numero_Departamento,
           CONCAT(D.Tamaño, ' m²') AS Tamaño,
           D.NumHabitaciones       AS Num_Habitaciones,
           D.Estado,
           C.Nombre                AS Nombre_Condominio,
           B.CantidadPisos         AS Pisos_Bloque,
           B.id_Condominio         AS ID_Condominio
    FROM Departamento D
             INNER JOIN Piso P ON D.id_Piso = P.id_Piso
             INNER JOIN Bloque B ON P.id_Bloque = B.id_Bloque
             INNER JOIN Condominio C ON B.id_Condominio = C.id_Condominio
    WHERE B.CantidadPisos > 7
      AND C.Direccion = 'Calle 123'
      AND D.Estado = 'Ocupado'
      AND D.Tamaño > 80;

SELECT *
FROM VistaDepartamentosBloquesGrandes;

-- # 6

CREATE VIEW VistaPropietariosExpensasVencidas AS
SELECT P.id_Propietario,
       P.Nombre,
       P.Apellido,
       P.FechaNacimiento,
       P.Telefono,
       P.CorreoElectronico,
       E.id_Expensas,
       E.Monto,
       E.FechaVencimiento
FROM Propietario P
         INNER JOIN Departamento D ON P.id_Propietario = D.id_Propietario
         INNER JOIN Expensas E ON D.id_Departamento = E.id_Departamento
WHERE E.FechaVencimiento < CURDATE()
  AND YEAR(P.FechaNacimiento) < 1980
  AND E.Monto > 200;

SELECT *
FROM VistaPropietariosExpensasVencidas;

ALTER VIEW VistaPropietariosExpensasVencidas AS
    SELECT P.id_Propietario                            AS ID_Propietario,
           CONCAT(P.Nombre, ' ', P.Apellido)           AS Nombre_Completo,
           DATE_FORMAT(P.FechaNacimiento, '%Y-%m-%d')  AS Fecha_Nacimiento,
           P.Telefono                                  AS Telefono,
           P.CorreoElectronico                         AS Correo_Electronico,
           CONCAT('$', FORMAT(E.Monto, 2))             AS Monto_Expensas,
           DATE_FORMAT(E.FechaVencimiento, '%Y-%m-%d') AS Fecha_Vencimiento
    FROM Propietario P
             INNER JOIN Departamento D ON P.id_Propietario = D.id_Propietario
             INNER JOIN Expensas E ON D.id_Departamento = E.id_Departamento;

SELECT *
FROM VistaPropietariosExpensasVencidas;


-- # 6

CREATE VIEW VistaCondominios AS
SELECT c.id_Condominio,
       c.Nombre AS NombreCondominio,
       c.Direccion,
       c.NumeroBloques,
       b.id_Bloque,
       b.CantidadPisos,
       p.id_Piso,
       d.id_Departamento,
       d.Tamaño,
       d.NumeroDepartamento,
       d.NumHabitaciones,
       d.Estado,
       e.id_Estacionamiento
FROM Condominio c
         LEFT JOIN Bloque b ON c.id_Condominio = b.id_Condominio
         LEFT JOIN Piso p ON b.id_Bloque = p.id_Bloque
         LEFT JOIN Departamento d ON p.id_Piso = d.id_Piso
         LEFT JOIN Estacionamiento e ON d.id_Departamento = e.id_Departamento
WHERE c.Direccion LIKE '%Calle%'
   OR c.NumeroBloques > 4;

SELECT *
FROM VistaCondominios;

ALTER VIEW VistaCondominios AS
    SELECT c.id_Condominio,
           CONCAT('Nombre del Condominio: ', c.Nombre)    AS DescripcionCondominio,
           CONCAT('Dirección: ', c.Direccion)             AS DetalleDireccion,
           CONCAT('Número de Bloques: ', c.NumeroBloques) AS DetalleBloques,
           b.id_Bloque,
           b.CantidadPisos,
           p.id_Piso,
           d.id_Departamento,
           d.Tamaño,
           d.NumeroDepartamento,
           d.NumHabitaciones,
           d.Estado,
           e.id_Estacionamiento
    FROM Condominio c
             LEFT JOIN Bloque b ON c.id_Condominio = b.id_Condominio
             LEFT JOIN Piso p ON b.id_Bloque = p.id_Bloque
             LEFT JOIN Departamento d ON p.id_Piso = d.id_Piso
             LEFT JOIN Estacionamiento e ON d.id_Departamento = e.id_Departamento
    WHERE e.id_Estacionamiento IS NULL;

SELECT *
FROM VistaCondominios;


ALTER VIEW VistaCondominios AS
    SELECT c.id_Condominio,
           c.Nombre                             AS NombreCondominio,
           COUNT(DISTINCT b.id_Bloque)          AS CantidadBloques,
           SUM(b.CantidadPisos)                 AS TotalPisosPorBloque,
           COUNT(DISTINCT e.id_Estacionamiento) AS CantidadEstacionamientos,
           COUNT(DISTINCT d.id_Departamento)    AS CantidadDepartamentos
    FROM Condominio c
             LEFT JOIN Bloque b ON c.id_Condominio = b.id_Condominio
             LEFT JOIN Piso p ON b.id_Bloque = p.id_Bloque
             LEFT JOIN Departamento d ON p.id_Piso = d.id_Piso
             LEFT JOIN Estacionamiento e ON d.id_Departamento = e.id_Departamento
    GROUP BY c.id_Condominio, c.Nombre;


-- PROCEDIMIENTOS ALMACENADOS DE LA TABLA             Propietario

-- # 1-. INSERT
DELIMITER //
CREATE PROCEDURE InsertarPropietario(
    IN p_id INT,
    IN p_Nombre VARCHAR(255),
    IN p_Apellido VARCHAR(255),
    IN p_FechaNacimiento DATE,
    IN p_Telefono VARCHAR(255),
    IN p_CorreoElectronico VARCHAR(255)
)
BEGIN
    INSERT INTO Propietario (id_Propietario, Nombre, Apellido, FechaNacimiento, Telefono,
                             CorreoElectronico)
    VALUES (p_id, p_Nombre, p_Apellido, p_FechaNacimiento, p_Telefono, p_CorreoElectronico);
END;
//
DELIMITER ;

CALL InsertarPropietario(15, 'Juan', 'Perez', '1980-05-15', '123-456-7890', 'juan@example.com');

SELECT *
FROM Propietario;

-- # 2-. UPDATE

DELIMITER //
CREATE PROCEDURE ActualizarPropietario(
    IN p_id INT,
    IN p_Nombre VARCHAR(255),
    IN p_Apellido VARCHAR(255),
    IN p_FechaNacimiento DATE,
    IN p_Telefono VARCHAR(255),
    IN p_CorreoElectronico VARCHAR(255)
)
BEGIN
    UPDATE Propietario
    SET Nombre            = p_Nombre,
        Apellido          = p_Apellido,
        FechaNacimiento   = p_FechaNacimiento,
        Telefono          = p_Telefono,
        CorreoElectronico = p_CorreoElectronico
    WHERE id_Propietario = p_id;
END;
//
DELIMITER ;

CALL ActualizarPropietario(5, 'Juan', 'Perez', '1980-05-15', '123-456-7890', 'juan@example.com');


-- # 3-. DELETE
DELIMITER //
CREATE PROCEDURE EliminarPropietario(IN p_id INT)
BEGIN
    DELETE FROM Propietario WHERE id_Propietario = p_id;
END;
//
DELIMITER ;

CALL EliminarPropietario(15);

-- # 4-. SELECT

DELIMITER //
CREATE PROCEDURE ObtenerPropietario(IN p_id INT)
BEGIN
    SELECT * FROM Propietario WHERE id_Propietario = p_id;
END;
//
DELIMITER ;

CALL ObtenerPropietario(5);


-- # 5-.

-- # 6

-- BACKUP DE LA TABLA Propietario

CREATE TABLE IF NOT EXISTS backupPropietario
(
    id_Backup         int PRIMARY KEY AUTO_INCREMENT,
    id_Propietario    int,
    Nombre            varchar(50),
    Apellido          varchar(50),
    FechaNacimiento   date,
    Telefono          varchar(20),
    CorreoElectronico varchar(100),
    FechaBackup       timestamp,
    Accion            varchar(10)
);

SELECT *
FROM backupPropietario;


-- TRIGGER

-- # 1-. INSERT
DELIMITER //
CREATE TRIGGER Before_Insert_Propietario
    BEFORE INSERT
    ON Propietario
    FOR EACH ROW
BEGIN
    INSERT INTO backupPropietario (id_Propietario,
                                   Nombre,
                                   Apellido,
                                   FechaNacimiento,
                                   Telefono,
                                   CorreoElectronico,
                                   FechaBackup,
                                   Accion)
    VALUES (NEW.id_Propietario,
            NEW.Nombre,
            NEW.Apellido,
            NEW.FechaNacimiento,
            NEW.Telefono,
            NEW.CorreoElectronico,
            NOW(),
            'INSERT');
END;
//
DELIMITER ;


-- # 2-. UPDATE ()

DELIMITER //
CREATE TRIGGER Before_Update_Propietario
    BEFORE UPDATE
    ON Propietario
    FOR EACH ROW
BEGIN
    INSERT INTO backupPropietario (id_Propietario,
                                   Nombre,
                                   Apellido,
                                   FechaNacimiento,
                                   Telefono,
                                   CorreoElectronico,
                                   FechaBackup,
                                   Accion)
    VALUES (OLD.id_Propietario,
            OLD.Nombre,
            OLD.Apellido,
            OLD.FechaNacimiento,
            OLD.Telefono,
            OLD.CorreoElectronico,
            NOW(),
            'UPDATE');
END;
//
DELIMITER ;


-- # 3-. DELETE

DELIMITER //
CREATE TRIGGER Before_Delete_Propietario
    BEFORE DELETE
    ON Propietario
    FOR EACH ROW
BEGIN
    INSERT INTO backupPropietario (id_Propietario,
                                   Nombre,
                                   Apellido,
                                   FechaNacimiento,
                                   Telefono,
                                   CorreoElectronico,
                                   FechaBackup,
                                   Accion)
    VALUES (OLD.id_Propietario,
            OLD.Nombre,
            OLD.Apellido,
            OLD.FechaNacimiento,
            OLD.Telefono,
            OLD.CorreoElectronico,
            NOW(),
            'DELETE');
END;
//
DELIMITER ;


-- Diferencias entre PROCEDURE Y FUNCTION
-- FUNCTION
--Una función en una base de datos tiene como propósito principal realizar cálculos o procesamientos específicos en los datos y devolver un resultado único. Puede aceptar parámetros como entrada, realizar operaciones basadas en esos parámetros y luego proporcionar un valor de retorno. Por ejemplo, La función ObtenerPropietariosPorEdad toma una edad como entrada, busca en la tabla Propietario aquellos cuya edad coincide con la proporcionada y devuelve sus nombres completos como resultado.
DELIMITER //
CREATE FUNCTION ObtenerPropietariosPorEdad(
    p_Edad INT
)
    RETURNS VARCHAR(255)
    DETERMINISTIC
BEGIN
    DECLARE p_NombreCompleto VARCHAR(255);

    SELECT CONCAT(Nombre, ' ', Apellido)
    INTO p_NombreCompleto
    FROM Propietario
    WHERE YEAR(CURRENT_DATE()) - YEAR(FechaNacimiento) = p_Edad;

    RETURN p_NombreCompleto;
END;
//
DELIMITER ;

SELECT ObtenerPropietariosPorEdad(35) AS NombreCompleto;


--PROCEDURE
--Un procedimiento almacenado, por otro lado, tiene un propósito diferente. Aunque también puede recibir parámetros, su principal función es ejecutar una serie de instrucciones o acciones en una secuencia específica. Esto puede incluir cambios en la base de datos, como agregar, actualizar o eliminar registros. Aunque un procedimiento almacenado puede devolver valores mediante parámetros de salida, su enfoque principal es realizar acciones más que calcular valores.

DELIMITER //
CREATE PROCEDURE ObtenerPropietariosPorEdad(
    IN p_Edad INT,
    OUT p_Nombres VARCHAR(255),
    OUT p_Apellidos VARCHAR(255)
)
BEGIN
    SELECT Nombre, Apellido
    INTO p_Nombres, p_Apellidos
    FROM Propietario
    WHERE YEAR(CURRENT_DATE()) - YEAR(FechaNacimiento) = p_Edad;
END;
//
DELIMITER ;

CALL ObtenerPropietariosPorEdad(35, @nombres, @apellidos);

SELECT @nombres AS Nombres, @apellidos AS Apellidos;


-- SET GLOBAL log_bin_trust_function_creators = 1;


--Vistas


/*
SELECT * FROM VistaCondominios;


CREATE VIEW VistaPropietarios AS
SELECT p.id_Propietario, p.Nombre, p.Apellido, p.FechaNacimiento, p.Telefono, p.CorreoElectronico,
       d.id_Departamento, d.NumeroDepartamento, d.Tamaño, d.NumHabitaciones, d.Estado
FROM Propietario p
LEFT JOIN Departamento d ON p.id_Propietario = d.id_Propietario;

SELECT * FROM VistaPropietarios;

CREATE VIEW VistaDepartamentosConExpensas AS
SELECT d.id_Departamento, d.NumeroDepartamento, d.Tamaño, d.NumHabitaciones, d.Estado,
       e.id_Expensas, e.Monto, e.FechaVencimiento
FROM Departamento d
LEFT JOIN Expensas e ON d.id_Departamento = e.id_Departamento;

SELECT * FROM VistaDepartamentosConExpensas;

CREATE VIEW VistaBloquesConCondominio AS
SELECT b.id_Bloque, b.CantidadPisos, c.id_Condominio, c.Nombre AS NombreCondominio
FROM Bloque b
LEFT JOIN Condominio c ON b.id_Condominio = c.id_Condominio;

SELECT * FROM VistaBloquesConCondominio;

CREATE VIEW VistaPisosConDepartamentos AS
SELECT p.id_Piso, b.id_Bloque, d.id_Departamento, d.NumeroDepartamento
FROM Piso p
LEFT JOIN Bloque b ON p.id_Bloque = b.id_Bloque
LEFT JOIN Departamento d ON p.id_Piso = d.id_Piso;

SELECT * FROM VistaPisosConDepartamentos;


SELECT D.NumeroDepartamento, CONCAT(P.Nombre, ' ', P.Apellido) AS
NombrePropietario
FROM Departamento AS D
INNER JOIN Propietario AS P ON D.id_Propietario = P.id_Propietario
INNER JOIN Expensas AS E ON D.id_Departamento = E.id_Departamento
WHERE D.Estado = 'Ocupado' AND E.Monto > 300.00;

SELECT P.Nombre, P.Apellido, COUNT(D.id_Departamento) AS CantidadDepartamentos
FROM Propietario AS P
LEFT JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
GROUP BY P.id_Propietario;
SELECT DISTINCT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
INNER JOIN Estacionamiento AS E ON D.id_Departamento = E.id_Departamento;
SELECT D.NumeroDepartamento, D.Tamaño
FROM Departamento AS D
WHERE D.Estado = 'Ocupado' AND D.NumHabitaciones > 3;
SELECT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
WHERE D.Estado = 'Desocupado';
/**
SELECT id_Condominio, Nombre, Direccion
FROM Condominio;
SELECT B.id_Bloque, B.CantidadPisos
FROM Bloque AS B;
SELECT id_Departamento, NumeroDepartamento
FROM Departamento
WHERE Estado = 'Ocupado';
SELECT id_Propietario, CONCAT(Nombre, ' ', Apellido) AS NombreCompleto
FROM Propietario;
SELECT E.id_Departamento, SUM(E.Monto) AS MontoTotalExpensas
FROM Expensas AS E
GROUP BY E.id_Departamento;
SELECT P.id_Propietario, CONCAT(P.Nombre, ' ', P.Apellido) AS NombreCompleto
FROM Propietario AS P
LEFT JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
WHERE D.id_Departamento IS NULL;
SELECT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
INNER JOIN Estacionamiento AS E ON D.id_Departamento = E.id_Departamento
GROUP BY P.id_Propietario
HAVING COUNT(E.id_Estacionamiento) > 1;
SELECT DISTINCT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
INNER JOIN Piso AS PS ON D.id_Piso = PS.id_Piso
INNER JOIN Bloque AS B ON PS.id_Bloque = B.id_Bloque
WHERE B.CantidadPisos > 8;

SELECT DISTINCT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
INNER JOIN Piso AS PS ON D.id_Piso = PS.id_Piso
INNER JOIN Bloque AS B ON PS.id_Bloque = B.id_Bloque
LEFT JOIN Estacionamiento AS E ON D.id_Departamento = E.id_Departamento
WHERE B.CantidadPisos > 8 AND E.id_Estacionamiento IS NULL;
SELECT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
WHERE D.Estado = 'Ocupado'
GROUP BY P.id_Propietario
HAVING COUNT(D.id_Departamento) > 1;
SELECT P.Nombre, P.Apellido
FROM Propietario AS P
INNER JOIN Departamento AS D ON P.id_Propietario = D.id_Propietario
INNER JOIN Condominio AS C1 ON D.id_Piso = C1.id_Condominio
WHERE EXISTS (
SELECT 1
FROM Departamento AS D2
INNER JOIN Condominio AS C2 ON D2.id_Piso = C2.id_Condominio
WHERE P.id_Propietario = D2.id_Propietario AND C1.id_Condominio <>
C2.id_Condominio
);
UPDATE Departamento
SET Estado = 'Desocupado'
WHERE id_Piso IN (
SELECT id_Piso
FROM Piso
WHERE id_Bloque = 1
);
UPDATE Bloque B
SET CantidadPisos = CantidadPisos + 1
WHERE (
SELECT COUNT(*) / B.CantidadPisos
FROM Departamento D
WHERE D.id_Piso = B.id_Bloque
) > 0.8;
UPDATE Departamento D
INNER JOIN Propietario P ON D.id_Propietario = P.id_Propietario
SET D.Estado = 'Desocupado'
WHERE YEAR(CURDATE()) - YEAR(P.FechaNacimiento) > 60;
UPDATE Expensas E
INNER JOIN Departamento D ON E.id_Departamento = D.id_Departamento
SET E.Monto = E.Monto * 1.15
WHERE D.Tamaño > 100;
UPDATE Departamento D
SET Estado = 'Moroso'
WHERE D.id_Departamento NOT IN (
SELECT id_Departamento
FROM Expensas
WHERE FechaVencimiento < CURDATE() AND Monto > 0
);
UPDATE Departamento
SET Tamaño = 75 + (NumHabitaciones * 15)
WHERE NumHabitaciones >= 3;
UPDATE Bloque B
SET CantidadPisos = CantidadPisos + 1
WHERE (
SELECT AVG(D.Tamaño)
FROM Departamento D
WHERE D.id_Piso = B.id_Bloque
) > 90;
DELETE FROM Expensas
WHERE FechaVencimiento < CURDATE();
DELETE FROM Departamento
WHERE Estado = 'Desocupado'
AND id_Departamento NOT IN (SELECT DISTINCT id_Departamento FROM Expensas);
DELETE FROM Propietario
WHERE id_Propietario NOT IN (SELECT DISTINCT id_Propietario FROM Departamento)
AND id_Propietario NOT IN (SELECT DISTINCT id_Propietario FROM Expensas);
DELETE FROM Estacionamiento
WHERE id_Departamento IS NULL OR id_Departamento NOT IN (
SELECT DISTINCT id_DepartamentoUPDATE Departamento D
INNER JOIN Propietario P ON D.id_Propietario = P.id_Propietario
SET D.Estado = 'Desocupado'
WHERE YEAR(CURDATE()) - YEAR(P.FechaNacimiento) > 60;
UPDATE Expensas E
INNER JOIN Departamento D ON E.id_Departamento = D.id_Departamento
SET E.Monto = E.Monto * 1.15
WHERE D.Tamaño > 100;
UPDATE Departamento D
SET Estado = 'Moroso'
WHERE D.id_Departamento NOT IN (
SELECT id_Departamento
FROM Expensas
WHERE FechaVencimiento < CURDATE() AND Monto > 0
);
UPDATE Departamento
SET Tamaño = 75 + (NumHabitaciones * 15)
WHERE NumHabitaciones >= 3;
UPDATE Bloque B
SET CantidadPisos = CantidadPisos + 1
WHERE (
SELECT AVG(D.Tamaño)
FROM Departamento D
WHERE D.id_Piso = B.id_Bloque
) > 90;
DELETE FROM Expensas
WHERE FechaVencimiento < CURDATE();
DELETE FROM Departamento
WHERE Estado = 'Desocupado'
AND id_Departamento NOT IN (SELECT DISTINCT id_Departamento FROM Expensas);
DELETE FROM Propietario
WHERE id_Propietario NOT IN (SELECT DISTINCT id_Propietario FROM Departamento)
AND id_Propietario NOT IN (SELECT DISTINCT id_Propietario FROM Expensas);
DELETE FROM Estacionamiento
WHERE id_Departamento IS NULL OR id_Departamento NOT IN (
SELECT DISTINCT id_Departamento
FROM Departamento
WHERE Estado = 'Ocupado'
);
DELETE FROM Departamento, Estacionamiento
USING Departamento
INNER JOIN Expensas ON Departamento.id_Departamento = Expensas.id_Departamento
LEFT JOIN Estacionamiento ON Departamento.id_Departamento =
Estacionamiento.id_Departamento
WHERE Expensas.FechaVencimiento < CURDATE();
DELETE FROM Propietario
WHERE id_Propietario NOT IN (
SELECT DISTINCT id_Propietario FROM Departamento
) AND id_Propietario NOT IN (
SELECT DISTINCT id_Propietario FROM Expensas
);
DELETE FROM Departamento, Expensas
USING Departamento
LEFT JOIN Expensas ON Departamento.id_Departamento = Expensas.id_Departamento
WHERE Departamento.Estado = 'Desocupado';
